/* global Modernizr, mapboxgl, mapEvents */

import enquire from 'enquire.js';
import SweetScroll from 'sweet-scroll';
import MicroModal from 'micromodal';
import { sprintf } from 'sprintf-js';

let map;
const mapShows = typeof mapEvents !== 'undefined' ? mapEvents : false;
const infoWindows = {};
const mapContainer = document.querySelector('#map');
const mapNav = document.querySelector('.map-nav');
const supportsGeolocation = 'geolocation' in navigator;
const geolocationPermission = localStorage.getItem('geolocation_permission');
let geolocationAllowed = geolocationPermission === null ? null : parseInt(geolocationPermission);
let latitude = false;
let longitude = false;

/**
 * Infowindows
 * @param  {Object} feature
 */
const openInfoWindow = (feature) => {
  map.flyTo({
    center: [ feature.geometry.coordinates[0], parseFloat(feature.geometry.coordinates[1]) + 0.005 ],
    zoom: 14
  });

  // Don't do anything if popup is already opened
  if (infoWindows.hasOwnProperty(feature.properties.id) && infoWindows[feature.properties.id].isOpen()) {
    return;
  }

  for (let id of Object.keys(infoWindows)) {
    const info = infoWindows[id];
    info.off('close', onInfoWindowClose);
    info.remove();
    delete infoWindows[id];
  }

  infoWindows[feature.properties.id] = new mapboxgl.Popup({
      closeOnClick: false,
      offset: [-1, -10],
      anchor: 'bottom'
    })
    .setLngLat(feature.geometry.coordinates)
    .setHTML(feature.properties.description)
    .addTo(map)
  ;

  // Update links
  updateLinks(infoWindows[feature.properties.id]._content);

  // Bind close
  infoWindows[feature.properties.id].on('close', onInfoWindowClose);
};

/**
 * Remove active
 */
const removeActive = () => {
  const activeItems = [...mapNav.querySelectorAll('.active')];
  activeItems && activeItems.forEach(item => {
    item.classList.remove('active');
  });
};

/**
 * Click event
 * @param  {Object} feature
 */
const onNavClick = (feature) => {
  // Open popup
  openInfoWindow(feature);
  // Remove active items from sidenav
  removeActive();
  // Add active item
  const navItems = [...mapNav.querySelectorAll(`[data-id="${feature.properties.id}"]`)];
  navItems.forEach(item => {
    item.classList.add('active');
    // Update links
    updateLinks(item);
  });
};

/**
 * On popup close
 * @param  {Object} popup
 */
const onInfoWindowClose = (popup) => {
  removeActive();
  fitBounds();
};

/**
 * [description]
 * @param  {HTMLelement} element
 */
const updateLinks = (element) => {
  if( !supportsGeolocation || !latitude || !longitude ) {
    return;
  }
  const geoVeloLinks = [...element.querySelectorAll('[data-url-geovelo]')];
  geoVeloLinks.forEach(link => {
    const url = link.getAttribute('data-url-geovelo');
    link.href = sprintf(url, `${latitude},${longitude}`);
  });
  const destineoLinks = [...element.querySelectorAll('[data-url-destineo]')];
  destineoLinks.forEach(link => {
    const url = link.getAttribute('data-url-destineo');
    link.href = sprintf(url, `${longitude};${latitude}`);
  });
};

/**
 * Handle geolocation error
 */
const onGeoError = (error, callback) => {
  typeof callback === 'function' && callback();
  const modal = document.querySelector('#map-modal-error');
  //   0: unknown error
  //   1: permission denied
  //   2: position unavailable (error response from location provider)
  //   3: timed out
  if (!modal) {
    return;
  }
  const modalContent = modal.querySelector('[data-modal-content]');
  if (!modalContent) {
    return;
  }
  const messages = [
    'erreur inconnue',
    'l’autorisation a été refusée',
    'la position n’est pas disponible',
    'délai expiré'
  ];

  // Save permission
  // Allow for all error types except permission denied
  if (geolocationAllowed === null || geolocationAllowed) {
    savePermission(error.code === 1 ? 0 : 1);
  }

  if (geolocationAllowed === null || (!!geolocationAllowed && error.code !== 1)) {
    modalContent.textContent = `Impossible de vous géolocaliser : ${messages[error.code]}`;
    MicroModal.show('map-modal-error');
  }
};

/**
 * Handle geoloaction success
 * @param  {Object} position
 */
const onGeoSuccess = (position, callback) => {
  // Save acceptation
  if (geolocationAllowed === null) {
    savePermission(1);
  }
  latitude = position.coords.latitude;
  longitude = position.coords.longitude;
  typeof callback === 'function' && callback();
};

/**
 * Get location
 * @param  {Function} callback
 */
const getLocation = (callback = false) => {
  navigator.geolocation.getCurrentPosition(
    (position) => onGeoSuccess(position, callback),
    (error) => onGeoError(error, callback), {
      timeout: 10 * 1000,
      maximumAge: 5 * 60 * 1000
    }
  );
};

/**
 * Save permission to localStorage
 * @param  {Number} value
 */
const savePermission = (value) => {
  localStorage.setItem('geolocation_permission', value);
};

/**
 * Bind events
 */
const bindEvents = () => {
  // Init map on load
  map.on('load', () => initMap());

  // Cursor on markers
  !Modernizr.touchevents && map.on('mouseenter', 'shows', () => {
    map.getCanvas().style.cursor = 'pointer';
  });
  !Modernizr.touchevents && map.on('mouseleave', 'shows', () => {
    map.getCanvas().style.cursor = '';
  });

  let scroll;

  // Bind markers
  map.on('click', 'shows', (e) => {
    const feature = e.features[0];
    onNavClick(feature);

    enquire.register('(min-width: 768px)', {
      match: () => {
        scroll = new SweetScroll({}, mapNav);
        scroll.toTop(`#item-${feature.properties.place_id}`);
      },
      unmatch: () => {
        scroll && scroll.destroy();
      }
    });
  });

  // Bind sidenav
  const mapNavItems = [...mapNav.querySelectorAll('[data-id]')];
  mapNavItems.forEach(item => {
    const handler = () => {
      const id = item.getAttribute('data-id');
      const feature = getFeature(id);
      feature && onNavClick(feature);
    };
    enquire.register('(min-width: 768px)', {
      match: () => item.addEventListener('click', handler),
      unmatch: () => item.removeEventListener('click', handler)
    });
  });

  supportsGeolocation && geolocationAllowed === null && [...document.querySelectorAll('[data-geolocation-deny]')].forEach(button => {
    const handler = (e) => {
      savePermission(0);
      e.target.removeEventListener(e.type, handler);
    };
    button.addEventListener('click', handler);
  });

  supportsGeolocation && geolocationAllowed === null && [...document.querySelectorAll('[data-geolocation-allow]')].forEach(button => {
    const handler = (e) => {
      e.target.removeEventListener(e.type, handler);
      e.target.classList.add('btn--loading');
      e.target.disabled = true;
      const callback = () => {
        MicroModal.close();
        e.target.disabled = false;
        e.target.classList.remove('btn--loading');
      };
      getLocation(callback);
    };
    button.addEventListener('click', handler);
  });
};

/**
 * Fit bounds
 */
const fitBounds = () => {
  // Fit bounds
  const coordinates = [];
  mapShows.features.forEach(feature => {
    coordinates.push(feature.geometry.coordinates);
  });

  const bounds = coordinates.reduce((bounds, coord) => {
    return bounds.extend(coord);
  }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

  map.fitBounds(bounds, {
    padding: 100
  });
};

/**
 * Get a feature by id
 * @param  {String} id
 * @return {Object}
 */
const getFeature = (id) => {
  var currentFeature = false;
  mapShows.features.forEach(function (feature) {
    if (feature.properties.id === id) {
      currentFeature = feature;
    }
  });
  return currentFeature;
};

/**
 * Init map
 */
const initMap = () => {
  map.addSource('shows', {
    'type': 'geojson',
    'data': mapShows
  });
  map.addLayer({
    'id': 'shows',
    'type': 'symbol',
    'source': 'shows',
    'layout': {
      'icon-image': 'marker-15',
      'icon-allow-overlap': true
    }
  });
};

/**
 * Build map
 */
const buildMap = () => {
  map = new mapboxgl.Map({
    container: mapContainer,
    style: 'mapbox://styles/mapbox/dark-v9',
    center: [-1.55, 47.23],
    zoom: 12
  });
  !Modernizr.touchevents && map.addControl(new mapboxgl.NavigationControl());

  // fitBounds();
};

/**
 * Init
 */
const init = () => {
  if (!mapNav || !mapContainer || !mapShows) {
    return;
  }
  buildMap();
  bindEvents();
  if (supportsGeolocation) {
    if (geolocationAllowed === null) {
      MicroModal.show('map-modal-location');
    }
    if (geolocationAllowed) {
      getLocation();
    }
  }
};

init();
