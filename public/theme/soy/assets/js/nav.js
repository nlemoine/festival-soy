const burger = document.querySelector('.js-nav-burger');

burger && burger.addEventListener('click', e => {
  burger.classList.toggle('open');
  document.documentElement.classList.toggle('nav--open');
});
