import Masonry from 'masonry-layout';
import imagesLoaded from 'imagesloaded';

const grid = document.querySelector('.archives-grid');

if( grid ) {
  const msnry = new Masonry( grid, {
    itemSelector: '.archive-item',
    // columnWidth: '.archive-sizer',
    percentPosition: true
  });

  imagesLoaded( grid ).on( 'progress', function() {
    msnry.layout();
  });
}
