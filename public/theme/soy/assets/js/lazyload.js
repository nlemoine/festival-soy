import 'lazysizes';
import 'lazysizes/plugins/respimg/ls.respimg';
import 'lazysizes/plugins/bgset/ls.bgset';

// Lazysizes config
window.lazySizesConfig = window.lazySizesConfig || {};
lazySizesConfig.loadMode = 1;
