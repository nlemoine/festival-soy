# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

set :application, 'festival-soy'
set :repo_url, 'git@bitbucket.org:nlemoine/festival-soy.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/homez.758/festivalbq/festival.soy'
set :tmp_dir, '/homez.758/festivalbq/festival.soy/tmp'

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
append :linked_files, 'public/.htaccess'

# Default value for linked_dirs is []
append :linked_dirs, 'public/files', 'public/thumbs', 'db'

# Files to copy from shared (can't symlink them)
set :copy_files, ['app/config/config_local.yml', 'app/config/extensions/*_local.yml']

# Files to keep across releases
set :keep_files, ['app/config/menu.yml', 'app/config/extensions/boltuioptions.snijder.yml', 'app/config/extensions/menu_backups']

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure

set :composer_install_flags, fetch(:composer_install_flags) + ' --classmap-authoritative --ignore-platform-reqs'

server 'ftp.cluster005.ovh.net', user: 'festivalbq'

namespace :composer do
  desc 'Runs composer'
  task :install do
    on roles(:all) do
      within release_path do
        invoke! 'composer:run', :install, fetch(:composer_install_flags), '-d extensions'
      end
    end
  end
end

namespace :app do
  task :copy_keep_files do
    next unless any? :keep_files
    on roles(:all) do
      within release_path do
        last_release = capture(:ls, '-xr', releases_path).split.fetch(1, nil)
        next unless last_release
        last_release_path = releases_path.join(last_release)
        fetch(:keep_files).each do |path|
          source = last_release_path.join(path)
          target = release_path.join(path)
          if test "[ -f #{source} ]"
            execute :cp, '-R', source, target
          elsif test "[ -d #{source} ]"
            execute :cp, '-R', source, target
          else
            warn "#{source} is not a file/dir that can be copied (target: #{target})"
          end
        end
      end
    end
  end
  task :copy_shared_files do
    next unless any? :copy_files
    on roles(:all) do
      within release_path do
        fetch(:copy_files).each do |path|
          source = shared_path.join(path)
          target = release_path.join(path)
          if test "[ -f #{source} ]"
            execute :cp, '-R', source, target
          elsif test "[ -d #{source} ]"
            execute :cp, '-R', source, target
          else
            warn "#{source} is not a file/dir that can be copied (target: #{target})"
          end
        end
      end
    end
  end
  task :clear_cache do
    on roles(:all) do
      within release_path do
        execute :php, 'app/nut', 'cache:clear'
      end
    end
  end
  task :update_database do
    on roles(:all) do
      within release_path do
        execute :php, 'app/nut', 'database:update'
      end
    end
  end
end

namespace :deploy do
  after :publishing, 'composer:install'
  before :published, 'app:copy_shared_files'
  before :published, 'app:copy_keep_files'
  after :published, 'app:clear_cache'
  after :published, 'app:update_database'
end
