import gulp from 'gulp'

import config from './config'
import plugins from './utils/plugins'
import handleErrors from './utils/handleErrors'

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// STYLES
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import packageImporter from 'node-sass-package-importer'
import autoprefixer from 'autoprefixer'
import postcssUrl from 'postcss-url'
import postCSSCustomProperties from 'postcss-custom-properties'
import cssnano from 'cssnano'

config.styles.enabled && gulp.task(config.styles.task, () => gulp
  .src(config.styles.src)
  .pipe(plugins.if(!config.isProduction, plugins.appendPrepend.prependText('$debug:true;')))
  .pipe(plugins.if(!config.isProduction, plugins.sourcemaps.init()))
  .pipe(plugins.sass({
    outputStyle: 'expanded',
    precision: 10,
    importer: packageImporter()
  }))
  .on('error', handleErrors)
  .pipe(plugins.postcss([
    autoprefixer,
    // require('postcss-will-change'),
    // require('css-mqpacker'),
    postcssUrl({
      basePath: path.join(path.resolve(config.buildPath), path.basename(config.buildPath)),
      url: 'inline',
      maxSize: 5,
      filter: '**/*.{jpg,jpeg,png,svg,gif}',
      optimizeSvgEncode: true
    }),
    postCSSCustomProperties(),
  ]))
  .pipe(plugins.if(config.isProduction, plugins.postcss([
    cssnano({
      reduceTransforms: false,
      reduceIdents: false,
      discardComments: {
        removeAll: true
      }
    })
  ])))
  .pipe(plugins.if(!config.isProduction, plugins.sourcemaps.write('.')))
  .pipe(gulp.dest(config.styles.dest))
  // .pipe(browserSync.stream({once: true}))
)


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// SCRIPTS (ES5)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

config.scriptsEs5.enabled && gulp.task(config.scriptsEs5.task, () => gulp
  .src(config.scriptsEs5.src)
  .pipe(plugins.uglify())
  // .pipe(plugins.if(config.isProduction, plugins.rev()))
  .pipe(gulp.dest(config.scriptsEs5.dest))
)

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// SCRIPTS (ES6)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// import rollup from 'rollup-stream'
import source from 'vinyl-source-stream'
import buffer from 'vinyl-buffer'
import betterRollup from 'gulp-better-rollup'
import rollupBabel from 'rollup-plugin-babel'
import rollupCommonjs from 'rollup-plugin-commonjs'
import rollupNodeResolve from 'rollup-plugin-node-resolve'
import rollupReplace from 'rollup-plugin-replace'
import rollupLegacy from 'rollup-plugin-legacy'

config.scriptsEs6.enabled && gulp.task(config.scriptsEs6.task, () => gulp
  .src(config.scriptsEs6.src)
  .pipe(plugins.sourcemaps.init())
  .pipe(betterRollup({
    cache: false,
    plugins: [
      rollupNodeResolve({
        jsnext: true,
        main: true,
        browser: true
      }),
      rollupCommonjs(),
      rollupBabel({
        babelrc: false,
        exclude: 'node_modules/**',
        presets: [
          [
            '@babel/preset-env',
            {
              modules: false,
              useBuiltIns: 'usage'
            }
          ]
        ]
      })
    ]
  }, {
    format: 'iife',
  }))
  .pipe(plugins.if(config.isProduction, plugins.uglify()))
  .pipe(plugins.if(!config.isProduction, plugins.sourcemaps.write('.')))
  .pipe(gulp.dest(config.scriptsEs6.dest))
)

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// MODERNIZR
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

config.modernizr.enabled && gulp.task(config.modernizr.task, () => gulp
  .src(config.modernizr.src, { since: gulp.lastRun(config.modernizr.task) })
  .pipe(plugins.modernizr(config.modernizr.options))
  .pipe(plugins.uglify())
  .pipe(gulp.dest(config.modernizr.dest))
)


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// IMAGES
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


import pngquant from 'imagemin-pngquant'
import zopfli from 'imagemin-zopfli'
import mozjpeg from 'imagemin-mozjpeg'
import giflossy from 'imagemin-mozjpeg'

config.images.enabled && gulp.task(config.images.task, () => gulp
  .src(config.images.src)
  .pipe(plugins.newer(config.images.dest))
  .pipe(plugins.imagemin([
    // GIF
    plugins.imagemin.gifsicle({
      interlaced: true,
      optimizationLevel: 3
    }),
    giflossy({
        optimizationLevel: 3,
        optimize: 3,
        lossy: 2
    }),
    // JPEG
    plugins.imagemin.jpegtran({
      progressive: true
    }),
    mozjpeg({
        quality: 90
    }),
    // PNG
    // plugins.imagemin.optipng({
    //   optimizationLevel: 7
    // }),
    pngquant({
        speed: 1,
        quality: 98
    }),
    zopfli({
        more: true
    }),
    // SVG
    plugins.imagemin.svgo({
      plugins: [
        {doctypeDeclaration: false},
        {namespaceIDs: false},
        {xmlDeclaration: false},
        {removeViewBox: true},
        {cleanupIDs: true}
      ]
    })
  ]))
  .pipe(plugins.size({title: 'images'}))
  .pipe(gulp.dest(config.images.dest))
)


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// SPRITE (SVG)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

config.spriteSvg.enabled && gulp.task(config.spriteSvg.task, () => gulp
  .src(config.spriteSvg.src)
  .pipe(plugins.svgSprite({
    mode: {
      symbol: {
        sprite: 'sprite.svg',
        dest: '.'
      }
    },
    shape: {
      id: {
        generator: '%s'
      }
    },
    svg: {
      xmlDeclaration: false,
      doctypeDeclaration: false,
      namespaceIDs: false
    }
  }))
  .on('error', handleErrors)
  .pipe(gulp.dest(config.spriteSvg.dest))
)


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// SPRITE (PNG)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// import pngquant from 'imagemin-pngquant'
// import zopfli from 'imagemin-zopfli'
// import buffer from 'vinyl-buffer'

const f = plugins.filter(['*.png'], { restore: true })

config.spritePng.enabled && gulp.task(config.spritePng.task, () => gulp
  .src(config.spritePng.src)
  .pipe(plugins.newer(path.join(config.spritePng.dest, 'sprite.png')))
  .pipe(plugins.spritesmith({
    imgName: 'sprite.png',
    cssName: '_sprite-smith.scss',
    imgPath: '../images/sprite.png',
    retinaImgName: 'sprite@2x.png',
    retinaImgPath: '../images/sprite@2x.png',
    retinaSrcFilter: config.spritePng.src2x,
    cssVarMap: (sprite) => {
      sprite.name = 's-' + sprite.name
    }
  }))
  .on('error', handleErrors)
  // .pipe(plugins.spritesmash())
  .pipe(f)
  .pipe(buffer())
  .pipe(plugins.imagemin([
    // PNG
    // plugins.imagemin.optipng({
    //   optimizationLevel: 7
    // }),
    pngquant({
        speed: 1,
        quality: 98
    }),
    zopfli({
        more: true
    })
  ]))
  .pipe(gulp.dest(config.spritePng.dest))
  .pipe(f.restore)
  .pipe(plugins.filter(['*.scss']))
  .pipe(gulp.dest(config.spritePng.destScss))
)


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// FONTS
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

config.fonts.enabled && gulp.task(config.fonts.task, () => gulp
  .src(config.fonts.src)
  .pipe(plugins.newer(config.images.dest))
  .pipe(gulp.dest(config.fonts.dest))
)


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// CLEAN
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import del from 'del'

gulp.task('clean', (done) => {
  del(config.buildPath, { dot: true, force: true }).then(() => {
    done()
  })
})


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// BUILD
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

gulp.task('compile', gulp.series(...getBuildTasks()))

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// WATCH
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import fs from 'fs'

!config.isProduction && gulp.task('watch', gulp.series('compile', () => {
  const tasks = getEnabledTasks()
  Object.values(tasks).forEach((task) => {
    let src = task.src
    // If src is a single file, watch all files with the same ext in that directory
    if (fs.existsSync(src)) {
      src = path.join(path.dirname(src), `**/*${path.extname(src)}`)
    }
    gulp.watch(src).on('change', gulp.series(task.task, () => {
      browserSync.reload(task.hasOwnProperty('behavior') && task.behavior === 'inject' ? '*.css' : task.dest)
    }))
  })
  gulp.watch([path.join(config.basePath, '**/*.{php,twig}')]).on('change', browserSync.reload);
}))


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// SERVE
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import browserSync from 'browser-sync'

gulp.task('serve', (done) => {
  browserSync.init({
    open: false,
    minify: true,
    proxy: `${config.pkg.name}.test`
  }, done)
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// VERSIONING
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import path from 'path'
import { getEnabledTasks, getRevisionedTasks, getDestPaths, getBuildTasks } from './utils/tasks'
import revDel from 'rev-del'

config.isProduction && gulp.task('build', gulp.series('compile', () => {

  // Do not include reved files, md5/hex/10
  const filterReved = plugins.filter(file => !/-[a-f0-9]{10}\./g.test(file.path))

  // Get enabled tasks
  const tasks = getRevisionedTasks()
  const dests = getDestPaths(tasks)
  return gulp
    .src(dests, {
      base: config.buildPath
    })
    .pipe(filterReved)
    .pipe(plugins.rev())
    .pipe(plugins.revCssUrl())
    // .pipe(plugins.if(config.isProduction, plugins.rename({ suffix: '.rev' })))
    // .pipe(plugins.revDeleteOriginal())
    .pipe(gulp.dest(config.buildPath))
    .pipe(plugins.rev.manifest('manifest.json'))
    .pipe(revDel({ dest: config.buildPath }))
    .pipe(gulp.dest(config.buildPath))

}))

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// DEFAULT
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

!config.isProduction && gulp.task('default', gulp.series('serve', 'watch'))
