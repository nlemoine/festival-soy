import fs from 'fs'
import path from 'path'

const pkg          = JSON.parse(fs.readFileSync('./package.json'))
const basePath     = path.join('public', 'theme', 'soy')
const assetsPath   = path.join(basePath, 'assets')
const buildPath    = path.join('public', 'assets')
const env          = process.env.NODE_ENV || 'development'
const isProduction = env === 'production'

const config = {
  pkg: pkg,
  basePath: basePath,
  assetsPath: assetsPath,
  buildPath: buildPath,
  isProduction: isProduction,
  // JS (ES5)
  scriptsEs5: {
    order: 1,
    task: 'scripts-es5',
    enabled: false,
    revision: false,
    behavior: 'reload',
    src: path.join(assetsPath, 'js', 'es5', '**/*.js'),
    dest: path.join(buildPath, 'js')
  },
  // JS (ES6)
  scriptsEs6: {
    order: 1,
    task: 'scripts-es6',
    enabled: true,
    revision: true,
    behavior: 'reload',
    src: path.join(assetsPath, 'js', 'app.js'),
    dest: path.join(buildPath, 'js')
  },
  // CSS
  styles: {
    order: 10,
    task: 'styles',
    enabled: true,
    revision: true,
    behavior: 'inject',
    src: path.join(assetsPath, 'css', '**/*.scss'),
    dest: path.join(buildPath, 'css')
  },
  // SPRITE (SVG)
  spriteSvg: {
    order: 1,
    task: 'sprite-svg',
    enabled: true,
    revision: true,
    behavior: 'reload',
    src: path.join(assetsPath, 'images', 'sprite-svg', '*.svg'),
    dest: path.join(buildPath, 'images')
  },
  // SPRITE (PNG)
  spritePng: {
    order: 1,
    task: 'sprite-png',
    enabled: false,
    revision: true,
    behavior: 'reload',
    src: path.join(assetsPath, 'images', 'sprite-png', '*.png'),
    src2x: path.join(assetsPath, 'images', 'sprite-png', '*@2x.png'),
    dest: path.join(buildPath, 'images'),
    destScss: path.join(assetsPath, 'css', 'components')
  },
  // IMAGES
  images: {
    order: 1,
    task: 'images',
    enabled: true,
    revision: true,
    behavior: 'reload',
    src: [
      path.join(assetsPath, 'images', '*.{jpg,jpeg,gif,png,svg}')
    ],
    dest: path.join(buildPath, 'images')
  },
  // FONTS
  fonts: {
    order: 1,
    task: 'fonts',
    enabled: true,
    revision: true,
    behavior: 'reload',
    src: path.join(assetsPath, 'fonts', '*.{woff,woff2,eot,svg,ttf}'),
    dest: path.join(buildPath, 'fonts')
  },
  // MODERNIZR
  modernizr: {
    order: 1,
    task: 'modernizr',
    enabled: true,
    revision: false,
    behavior: 'reload',
    src: path.join(assetsPath, 'js', '*.js'),
    dest: path.join(buildPath, 'js'),
    options: {
      cache: true,
      crawl: false,
      parseFiles: false,
      customTests: [],
      tests: [
        'touchevents'
      ],
      options: [
        'setClasses'
      ]
    }
  }
}
export default config
