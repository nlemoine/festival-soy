<?php

namespace App\Twig\Extension;

use App\Twig\Runtime\ImagePlaceholderRuntime;
use Twig_SimpleFunction;
use Twig_Extension;

class ImagePlaceholderExtension extends Twig_Extension
{
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('img_placeholder', [ImagePlaceholderRuntime::class, 'imgPlaceholder']),
        ];
    }

    public function getName()
    {
        return 'image_placeholder';
    }
}
