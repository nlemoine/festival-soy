<?php

namespace HelloNico\Twig\Extension;

use HelloNico\Twig\Runtime\CacheBustingRuntime;
use Twig_SimpleFilter;
use Twig_Extension;

class CacheBustingExtension extends Twig_Extension
{
    public function getFilters()
    {
        return [
            new Twig_SimpleFilter('cachebusting', [CacheBustingRuntime::class, 'cacheBusting']),
        ];
    }

    public function getName()
    {
        return 'cachebusting';
    }
}
