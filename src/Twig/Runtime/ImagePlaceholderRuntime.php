<?php

namespace App\Twig\Runtime;

class ImagePlaceholderRuntime
{
    public function imgPlaceholder($width, $height)
    {
        $svg = sprintf("%%3Csvg xmlns='http://www.w3.org/2000/svg' width='%d' height='%d' viewBox='0 0 %d %d'%%3E%%3C/svg%%3E", $width, $height, $width, $height);
        return sprintf("data:image/svg+xml,%s", $svg);
    }
}
