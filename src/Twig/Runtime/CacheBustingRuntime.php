<?php

namespace HelloNico\Twig\Runtime;

class CacheBustingRuntime
{
    private $debug;
    private $web_path;

    public function __construct($debug, $web_path)
    {
        $this->debug = $debug;
        $this->web_path = $web_path;
    }

    public function cacheBusting($file)
    {
        $asset_path = pathinfo($file, PATHINFO_DIRNAME);

        $file_path = sprintf('%s%s', $this->web_path, $file);
        $file_parts = pathinfo($file_path);
        $file_name = sprintf('%s%s.%s', $file_parts['filename'], $this->debug ? '' : '.min', $file_parts['extension']);
        $file_path = sprintf('%s/%s', $file_parts['dirname'], $file_name);

        if (!is_file($file_path)) {
            return $file;
        }

        return sprintf('%s/%s?v=%d', $asset_path, $file_name, filemtime($file_path));
    }
}
