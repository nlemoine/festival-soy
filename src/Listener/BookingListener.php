<?php

namespace App\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Silex\Application;
use Bolt\Response\TemplateView;
use App\Repository\ArtistRepository;
use App\Entity\Artist;

class BookingListener implements EventSubscriberInterface
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function onBooking(GetResponseForControllerResultEvent $event)
    {
        $view = $event->getControllerResult();
        if (!$view instanceof TemplateView) {
            return;
        }

        $context = $view->getContext();
        if (!isset($context->record->contenttype['singular_slug'])) {
            return;
        }
        if ($context->record->contenttype['singular_slug'] !== 'booking') {
            return;
        }

        $repo = $this->app['storage']->getRepository(Artist::class);
        $concerts = $repo->findCurrentArtists($this->app['app.current_edition']->id, [
            'is_free' => false,
        ]);

        if (empty($concerts)) {
            $context->set('concerts', []);
            return;
        }
        $context->set('concerts', $this->getConcerts($concerts));
        $view->setContext($context);
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [];
    }

    private function getConcerts($artists)
    {
        $records = [];
        foreach ($artists as $artist) {
            $current_date = date('Y-m-d', strtotime($artist['datetime']));
            $records[$current_date.$artist['place']]['datetime']           = $artist['datetime'];
            $records[$current_date.$artist['place']]['booking_url']        = $artist['booking_url'];
            $records[$current_date.$artist['place']]['price']              = $artist['price'];
            $records[$current_date.$artist['place']]['artists'][] = [
                'title' => $artist['title'],
                'link' => $artist->link()
            ];
            $records[$current_date.$artist['place']]['is_full']            = $artist['is_full'];
        }

        return $records;
    }
}
