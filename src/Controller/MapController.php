<?php

namespace App\Controller;

use Bolt\Controller\Base;
use Bolt\Controller\Zone;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Artist;

class MapController extends Base
{

    /**
     * {@inheritdoc}
     */
    public function addRoutes(ControllerCollection $c)
    {
        $c->value(Zone::KEY, Zone::FRONTEND);
        $c->match('/', [$this, 'response']);

        return $c;
    }

    /**
     * @param Request $request
     * @param string  $type
     *
     * @return Response
     */
    public function response(Request $request)
    {
        $repo_artists = $this->app['storage']->getRepository(Artist::class);

        $artists = $repo_artists->findCurrentArtists($this->app['app.current_edition']->id);

        $records = [];
        $navs = [];
        if (!empty($artists)) {
            $places_ids = [];
            foreach ($artists as $artist) {
                if (!empty($artist['place']) && is_numeric($artist['place'])) {
                    $places_ids[] = $artist['place'];
                }
            }

            $repo_places = $this->app['storage']->getRepository('places');

            $records = [];
            $navs = [];
            foreach ($artists as $artist) {
                $place = $this->getPlace(intval($artist['place']));
                if (!$place) {
                    continue;
                }
                $lat = round(isset($place->geolocation['latitude']) ? $place->geolocation['latitude'] : 0, 5);
                $lng = round(isset($place->geolocation['longitude']) ? $place->geolocation['longitude'] : 0, 5);
                $index = $lat . $lng;
                $current_date = date('Y-m-d', strtotime($artist['datetime']));

                // Records for map
                $records[$index]['lat'] = $lat;
                $records[$index]['lng'] = $lng;
                $records[$index]['place_id'] = $artist['place'];
                $records[$index]['concerts'][$artist['place']]['meta'] = [
                    'place'         => $place,
                    'price'         => $artist['price'],
                    'price_mention' => $artist['price_mention'],
                    'time'          => $artist['datetime'],
                ];
                $records[$index]['concerts'][$artist['place']]['artists'][] = $artist;

                // Records for map sidebar
                $navs[$current_date][$artist['place']]['meta'] = [
                    'place' => $place,
                    'price' => $artist['price'],
                    'time'  => $artist['datetime'],
                    'index' => $index
                ];
                $navs[$current_date][$artist['place']]['artists'][] = $artist;
            }
        }

        return $this->render('map.twig', [], [
            'records' => $records,
            'navs' => $navs
        ]);
    }

    private function getPlace($place_id)
    {
        return $this->app['query']->getContent(sprintf('places/%d', $place_id));
    }

}
