<?php

namespace App\Controller;

use Bolt\Controller\Base;
use Bolt\Controller\Zone;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Artist;
use App\Entity\Place;

class LineUpController extends Base
{

    /**
     * {@inheritdoc}
     */
    public function addRoutes(ControllerCollection $c)
    {
        $c->value(Zone::KEY, Zone::FRONTEND);
        $c->match('/', [$this, 'response']);

        return $c;
    }

    /**
     * @param Request $request
     * @param string  $type
     *
     * @return Response
     */
    public function response(Request $request)
    {
        $repo_artists = $this->app['storage']->getRepository(Artist::class);
        $repo_places = $this->app['storage']->getRepository(Place::class);

        $artists = $repo_artists->findCurrentArtists($this->app['app.current_edition']->id);
        $records = [];

        if (!empty($artists)) {
            $places_ids = [];
            foreach ($artists as $artist) {
                if (!empty($artist['place']) && is_numeric($artist['place'])) {
                    $places_ids[] = $artist['place'];
                }
            }
            $places = [];
            // $places = $repo_places->findBy(['id' => $places_ids]);

            $records = [];
            foreach ($artists as $artist) {
                $current_date = date('Y-m-d', strtotime($artist['datetime']));
                $records[$current_date][$artist['place']]['meta'] = [
                    'place'         => $this->getPlaceTitle(intval($artist['place']), $places),
                    'price'         => $artist['price'],
                    'price_mention' => $artist['price_mention'],
                    'time'          => $artist['datetime'],
                ];
                $records[$current_date][$artist['place']]['artists'][] = $artist;
            }
        }

        return $this->render('lineup.twig', [], [
            'records' => $records,
        ]);
    }

    private function getPlaceTitle($place_id, $places)
    {
        $place = $this->app['query']->getContent(sprintf('places/%d', $place_id));
        return isset($place->title) ? $place->title : null;

        // $repo_places = $this->app['storage']->getRepository(Place::class);
        // $place = $repo_places->findOneBy(['id' => $place_id]);
        // return $place->title;

        foreach ($places as $place) {
            if ($place->id !== $place_id) {
                continue;
            }
            return $place['title'];
        }
    }
}
