<?php

namespace App;

use Silex\Application;
use Bolt\Extension\SimpleExtension;
use App\Twig\Runtime\ImagePlaceholderRuntime;
use App\Twig\Extension\ImagePlaceholderExtension;
use App\Controller\LineUpController;
use App\Controller\MapController;
use Pimple as Container;
use App\Nut\ImportArtists;
use App\Listener\ExceptionListener;
use App\Listener\BookingListener;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Bolt\Extension\StorageTrait;
use App\Entity\Artist;
use App\Entity\Place;
use App\Repository\ArtistRepository;
use App\Repository\PlaceRepository;
use App\Asset\Schema;
use Bolt\Asset\Snippet\Snippet;
use Bolt\Asset\Target;

class Boot extends SimpleExtension
{
    use StorageTrait;

    /**
     * {@inheritdoc}
     */
    protected function registerAssets()
    {
        $app = $this->getContainer();
        $schema = new Schema($app);

        $asset = Snippet::create()
            ->setCallback([$schema, 'snippet'])
            ->setLocation(Target::AFTER_META)
            ->setPriority(5)
        ;

        return [
            $asset,
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function subscribe(EventDispatcherInterface $dispatcher)
    {
        // $this->getContainer()['config']->get('general/canonical')
        $app = $this->getContainer();
        $exceptionListener = new ExceptionListener($app);
        $bookingListener = new BookingListener($app);
        $dispatcher->addListener(KernelEvents::EXCEPTION, [$exceptionListener, 'onException']);
        $dispatcher->addListener(KernelEvents::VIEW, [$bookingListener, 'onBooking']);
    }

    /**
     * {@inheritdoc}
     */
    protected function registerRepositoryMappings()
    {
        return [
            'artists' => [
                Artist::class => ArtistRepository::class
            ],
            // 'places' => [
            //     Place::class => PlaceRepository::class
            // ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function registerFrontendControllers()
    {
        return [
            '/programmation' => new LineUpController(),
            // '/billetterie' => new BookingController(),
            '/carte-interactive' => new MapController(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function registerTwigPaths()
    {
        return [
            'public' => [
                'position' => 'prepend',
                'namespace' => '__main__'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function registerServices(Application $app)
    {
        $this->extendRepositoryMapping();

        $app['app.current_edition'] = function () use ($app) {
            $repo = $app['storage']->getRepository('editions');
            return $repo->findOneBy([], ['id', 'DESC']);
        };

        $app['twig.runtime.image_placeholder'] = function () {
            return new ImagePlaceholderRuntime();
        };

        $app['twig.runtimes'] = $app->extend('twig.runtimes', function (array $runtimes) {
            return $runtimes + [
                // CacheBustingRuntime::class => 'twig.runtime.cachebusting',
                ImagePlaceholderRuntime::class => 'twig.runtime.image_placeholder'
            ];
        });

        $app['twig.extension.image_placeholder'] = $app->share(function () {
            return new ImagePlaceholderExtension();
        });

        $app['twig'] = $app->share($app->extend('twig', function ($twig) use ($app) {
            $twig->addGlobal('festival', $app['app.current_edition']);
            $twig->addExtension($app['twig.extension.image_placeholder']);
            return $twig;
        }));
    }

    /**
     * {@inheritdoc}
     */
    protected function registerNutCommands(Container $container)
    {
        return [
            // new ExtractVideos($container),
            // new ImportEditions($container),
            // new ImportArtists($container),
        ];
    }
}
