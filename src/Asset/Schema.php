<?php

namespace App\Asset;

use Silex\Application;
use Bolt\Helpers\Html;
use Spatie\SchemaOrg\Schema as SchemaOrg;
use DateTime;
use Spatie\SchemaOrg\MusicEvent;
use Spatie\SchemaOrg\Place;

class Schema
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Snippet dispatcher
     * @return string
     */
    public function snippet()
    {
        $vars = $this->app['twig']->getGlobals();

        // Dispatch

        // Artist
        if (isset($vars['record']) && $vars['record']->contenttype['slug'] === 'artists') {
            return $this->musicEvent($vars['record']->values);
        }

        // Lineup
        // if (isset($vars['lineup']) && $this->app['request']->getPathInfo() === '/programmation') {
        //     return $this->schemaLineup(array_column($vars['lineup'], 'values'));
        // }
    }

    /**
     * Build schema for lineup
     * @param  array $artists
     * @return string
     */
    private function schemaLineup($artists)
    {

        // Get min/max dates
        $dates = array_column($artists, 'datetime');
        sort($dates);
        $start_date = new DateTime(reset($dates));
        $end_date = new DateTime(end($dates));

        // @todo fill missing fields
        $event = SchemaOrg::MusicEvent()
            ->name(sprintf('%s - %s', $this->app['config']->get('general/sitename'), $this->app['config']->get('general/payoff')))
            ->startDate($start_date->format('c'))
            ->endDate($end_date->format('c'))
            // ->url()
            // ->image()
            // ->description()
            // ->offers()
            ->location(
                SchemaOrg::Place()
                    ->name('Nantes')
                    ->address(
                        SchemaOrg::PostalAddress()
                            ->addressLocality('Nantes')
                            ->addressCountry('France')
                    )
            )
            ->performer(array_values(array_map(function ($artist) {
                return SchemaOrg::MusicGroup()
                    ->name($artist['title'])
                ;
            }, $artists)))
        ;

        return $event->toScript();
    }

    /**
     * Build schema for artist
     * @param  array $artist
     * @return string
     */
    private function musicEvent($artist)
    {
        $location = $this->getLocation($artist['place']);

        $date = new DateTime($artist['datetime']);

        $event = SchemaOrg::MusicEvent()
            ->name($artist['title'])
            ->url($this->app['canonical']->getUrl())
            ->performer(SchemaOrg::MusicGroup()->name($artist['title']))
            ->image(sprintf(
                '%s%s',
                $this->app['paths']['canonical'],
                $this->app['twig.runtime.bolt_image']->thumbnail($artist['image']['file'], 1920, 1200)
            ))
            ->description(Html::trimText(strip_tags($artist['body']), 170))
            ->startDate($date->format('c'))
            ->endDate($date->modify('+2 hours')->format('c'))
            ->if($location, function (MusicEvent $event) use ($location) {
                $event->location($location);
            })
            ->if($artist['price'], function (MusicEvent $event) use ($artist) {
                $event->offers($this->getOffers($artist));
            });
        ;

        return $event->toScript();
    }

    /**
     * Get offers schema
     * @param  array $artist
     * @return boolean|object
     */
    private function getOffers($artist)
    {
        $prices = [];
        if ($artist['is_free']) {
            $prices[] = 0;
        } else {
            preg_match_all('/\d+/', $artist['price'], $matches);
        }

        if (isset($matches[0]) && !empty($matches[0])) {
            $prices = $matches[0];
        }

        if (empty($prices)) {
            return false;
        }

        // @TODO get global booking URL
        $booking_url = !empty($artist['booking_url']) ? $artist['booking_url'] : 'http://digitick.com/';

        $offers = [];
        foreach ($prices as $offer) {
            $offers[] = SchemaOrg::Offer()
                ->price($offer)
                ->priceCurrency('EUR')
                ->url($booking_url)
                ->availability($artist['is_full'] ? 'http://schema.org/OutOfStock' : 'http://schema.org/InStock')
            ;
        }

        return count($offers) === 1 && isset($offers[0]) ? $offers[0] : $offers;
    }

    /**
     * Get location schema
     * @param  int $place_id
     * @return boolean|object
     */
    private function getLocation($place_id)
    {
        // Get place
        $place = empty($place_id) ? false : $this->app['query']->getContent('places', [
            'id'           => $place_id,
            'returnsingle' => true,
        ])->toArray();

        return !$place ? false : SchemaOrg::Place()
            ->name($place['title'])
            ->if(isset($place['geolocation']['formatted_address']), function (Place $location) use ($place) {
                $location->address($place['geolocation']['formatted_address']);
            })
            ->if(isset($place['geolocation']['latitude']) && isset($place['geolocation']['longitude']), function (Place $location) use ($place) {
                $geo = SchemaOrg::GeoCoordinates()
                    ->latitude($place['geolocation']['latitude'])
                    ->longitude($place['geolocation']['longitude'])
                ;
                $location->geo($geo);
            })
        ;
    }
}
