<?php

namespace App\Repository;

use Bolt\Storage\Repository\ContentRepository;
use Doctrine\DBAL\Query\QueryBuilder;

class ArtistRepository extends ContentRepository
{
    public function findCurrentArtists($edition, $where = [])
    {
        $qb = $this->createQueryBuilder('a')
            ->join('a', 'bolt_relations', 'r', '
                r.from_contenttype = "artists" AND
                r.from_id = a.id AND
                r.to_contenttype = "editions"
            ')
            ->andWhere('a.status = :status')->setParameter('status', 'published')
            ->andWhere('r.to_id = :edition')->setParameter('edition', $edition)
            ->addOrderBy('a.datetime', 'asc')
        ;

        foreach ($where as $field => $value) {
            $qb->andWhere(sprintf('a.%s = :%s', $field, $field))->setParameter($field, $value);
        }

        $artists = $this->findWith($qb);

        return $artists;
    }


    /**
     * @param string $alias
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = null)
    {
        /*
         * It's very important to override the createQueryBuilder method because
         * the parent class force the alias to "content" which is not compatible
         * with our custom entity.
         */
        if (empty($alias)) {
            $alias = $this->getAlias();
        }

        return parent::createQueryBuilder($alias);
    }
}
