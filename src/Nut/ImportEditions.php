<?php

namespace HelloNico\Nut;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Bolt\Storage\Entity\Content;
use Bolt\Application;
use League\Csv\Reader;

class ImportEditions extends Command
{
    public $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->app['request'] = false;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('import:editions')
            ->setDescription('Import SOY editions')
            // ->addArgument(
            //     'file',
            //     InputArgument::OPTIONAL,
            //     'CSV file'
            // )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        // $file = $input->getArgument('file');
        // if( !is_file($file) ) {
        //     $output->writeln('<error>No file</error>');
        //     return;
        // }

        $editions_file = file_get_contents('https://docs.google.com/spreadsheets/d/e/2PACX-1vRjHV2Nc4WgrCNjj8j23bpuD3lPfTreeGp006eMMRMwKKHQ6rTIvDmyAru-ND0epGmF4K5LZdDA4TKv/pub?gid=769810125&single=true&output=csv');

        // $editions_file = file_get_contents($file);

        $csv = Reader::createFromString($editions_file);
        $csv->setHeaderOffset(0);
        $editions = $csv->getRecords();

        $this->app['db']->executeQuery('TRUNCATE TABLE bolt_editions');

        foreach ($editions as $id => $edition) {
            $repo = $this->app['storage']->getRepository('editions');
            $record = $this->app['storage']->getContent('editions', ['id' => $id]);
            if (!$record) {
                $record = new Content();
            }
            $record->setValues([
                'title'       => $edition['title'],
                'status'      => 'published',
                'slug'        => sprintf('festival-soy-%d', $id),
                'datepublish' => date('Y-m-d H:i:s'),
                'start_date'  => $edition['start_date'],
                'end_date'    => $edition['end_date'],
                'image'       => ['file' => sprintf('archives/soy-%d.jpg', $id)]
            ]);

            $repo->save($record);
            $output->writeln(sprintf('<comment>Successfully created SOY %d</comment>', $id));
        }

        $output->writeln('<info>All editions have been created</info>');
    }
}
