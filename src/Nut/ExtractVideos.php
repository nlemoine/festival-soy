<?php

namespace HelloNico\Nut;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Bolt\Application;
use Sunra\PhpSimple\HtmlDomParser;
use Bolt\Exception\EmbedResolverException;
use GuzzleHttp\Psr7;

class ExtractVideos extends Command
{
    public $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->app['request'] = false;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('extract:videos')
            ->setDescription('Extracts iframe and record it as video field');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $artists = $this->app['storage']->getContent('artists', [
            'limit' => 1000,
            'status' => 'held || published'
        ]);

        $videos = [];
        foreach ($artists as $item) {

            // Parse body
            $html = HtmlDomParser::str_get_html($item->values['body']);
            $iframe = $html->find('iframe', 0);
            if (!$iframe) {
                continue;
            }
            if (false === strpos($iframe->src, 'youtube.com')) {
                continue;
            }

            $output->writeln('<comment>Extracting video for ' . $item['title'] . '</comment>');

            // Get YouTube video infos
            $tmp = explode('/', $iframe->src);
            $youtube_id = end($tmp);
            $youtube_url = sprintf('https://www.youtube.com/watch?v=%s', $youtube_id);

            // Remove iframe parent
            // $iframe->parent()->outertext = '';
            // Remove iframe
            $iframe->outertext = '';

            $output->writeln('<comment>Getting oEmbed for ' . $youtube_url . '</comment>');

            // Get oembed data
            $resolver = $this->app['embed'];
            try {
                $data = $resolver->embed(new Psr7\Uri($youtube_url), 'oembed');
            } catch (EmbedResolverException $e) {
                $response = ['error' => ['message' => $e->getMessage()]];
                $output->writeln('<error>'.$e->getMessage().'</error>');
            }

            $video = [
                'url'        => $youtube_url,
                'width'      => $data['width'],
                'height'     => $data['height'],
                'title'      => $data['title'],
                'ratio'      => $data['width'] / $data['height'],
                'authorname' => $data['author_name'],
                'authorurl'  => $data['author_url'],
                'html'       => $data['html'],
                'thumbnail'  => $data['thumbnail_url'],
            ];

            // Save data
            $item->setValues([
                'body' => $html->save(),
                'video' => $video,
            ]);
            $this->app['storage']->saveContent($item);

            $output->writeln('<info>Video successfully saved for ' . $item['title'] . '</info>');
        }
        $output->writeln('<info>All videos have been extracted</info>');
    }
}
