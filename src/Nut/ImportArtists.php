<?php

namespace App\Nut;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Style\SymfonyStyle;
use Bolt\Application;
use Sunra\PhpSimple\HtmlDomParser;
use Bolt\Exception\EmbedResolverException;
use GuzzleHttp\Psr7;
use League\Csv\Reader;
use League\Csv\Statement;
use Fuse\Fuse;
use Cocur\Slugify\Slugify;
use Psr\Log\LogLevel;
use Google_Client;
use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;
use Google_Service_Sheets_BatchUpdateValuesRequest;
use DateTime;

class ImportArtists extends Command
{
    public $app;
    private $artists;
    const FIELD_MAPPING = [
        'id'           => 0,
        'title'        => 1,
        'datetime'     => 2,
        'style'        => 3,
        'music_label'  => 4,
        'origin'       => 5,
        'price'        => 6,
        'edition'      => 7,
        'place'        => 8,
        'is_cancelled' => 9,
    ];

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->app['request'] = false;
        $this->artists = $this->app['storage']->getContent('artists');
        $this->places = $this->app['storage']->getContent('places');
        // $this->artists = array_map(function($value) {
        //     return array_filter($value, function($value, $key) {
        //         return in_array($key, ['title'], true);
        //     }, ARRAY_FILTER_USE_BOTH);
        // }, array_column($this->artists, 'values'));
        $this->places = array_column($this->places, 'values');
        $this->artists = array_column($this->artists, 'values');
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('import:artists')
            ->setDescription('Import SOY artists from Google Speadsheet');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        // Get the API client and construct the service object.
        $client = $this->getClient();
        $service = new Google_Service_Sheets($client);

        $spreadsheet_id = '15t3tBKBcAGcnn3naQEdgahM2XHNEZ-mLaGJ9Oz0UfRk';
        $line_start     = 2;
        $range          = sprintf('artistes!A%d:L500', $line_start);
        $response       = $service->spreadsheets_values->get($spreadsheet_id, $range);
        $rows           = $response->getValues();

        // Add line number as index
        // and filter empty rows
        $artists_remote = [];
        foreach($rows as $row) {
            if (!empty(array_filter($row))) {
                $artists_remote[$line_start] = $row;
            }
            $line_start += 1;
        }

        $repo = $this->app['storage']->getRepository('artists');

        dump($artists_remote);

        // INSERT NEW ARTISTS
        foreach($artists_remote as $line => $artist) {
            // empty id
            if( !empty($artist[0] || empty($artist[1]) ) ) {
                continue;
            }

            $place_id = $this->getPlaceId(isset($artist[$this::FIELD_MAPPING['place']]) ? $artist[$this::FIELD_MAPPING['place']] : null);

            // Save artist
            preg_match('/\d+/m', $artist[$this::FIELD_MAPPING['edition']], $matches);
            $edition      = $matches[0];
            $date         = DateTime::createFromFormat('Y-m-d H:i:s', trim($artist[$this::FIELD_MAPPING['datetime']]));
            $date_publish = DateTime::createFromFormat('Y-m-d H:i:s', trim($artist[$this::FIELD_MAPPING['datetime']]))->modify('-1 month');
            $entity = $repo->create([
                'datecreated'   => $date_publish->format('Y-m-d H:i:s'),
                'datepublish'   => $date_publish->format('Y-m-d H:i:s'),
                'datechanged'   => $date_publish->format('Y-m-d H:i:s'),
                'datedepublish' => null,
                'status'        => 'published',
                'ownerid'       => 2,
            ]);
            $entity->setValues([
                'datetime'     => $date->format('Y-m-d H:i:s'),
                'title'        => $artist[$this::FIELD_MAPPING['title']],
                'slug'         => Slugify::create()->slugify($artist[$this::FIELD_MAPPING['title']]),
                'style'        => $artist[$this::FIELD_MAPPING['style']],
                'origin'       => $artist[$this::FIELD_MAPPING['origin']],
                'music_label'  => $artist[$this::FIELD_MAPPING['music_label']],
                'is_cancelled' => intval($artist[$this::FIELD_MAPPING['is_cancelled']])),
                'replaced_by'  => null,
                'place'        => $place_id,
                'is_free'      => stripos($artist[$this::FIELD_MAPPING['price']], 'gratuit') === false ? 0 : 1,
                'video'        => null,
                'image'        => null,
            ]);
            $entity->getRelation()->setFromPost([
                'relation' => [
                    'editions' => [$edition],
                ]
            ], $entity);
            $repo->save($entity);
        }




        // $data = [];
        // foreach($artists_remote as $line => $artist) {

        //     $artist_record = $this->app['query']->getContent('artists', [
        //         'title'        => $artist[$this::FIELD_MAPPING['title']],
        //         'returnsingle' => true,
        //         // 'printquery'   => true,
        //     ]);

        //     if(empty($artist_record) || $artist[$this::FIELD_MAPPING['title']] !== $artist_record['title'] ) {
        //         continue;
        //     }

        //     $range = sprintf('artistes!J%d', $line);
        //     $data[] = new Google_Service_Sheets_ValueRange([
        //         'range'          => $range,
        //         'values'         => [
        //             'values' => $artist_record['is_cancelled'],
        //         ],
        //     ]);
        // }

        // $body = new Google_Service_Sheets_BatchUpdateValuesRequest([
        //   'valueInputOption' => 'USER_ENTERED',
        //   'data'             => $data,
        // ]);
        // $result = $service->spreadsheets_values->batchUpdate($spreadsheet_id, $body);

        // $io = new SymfonyStyle($input, $output);
        // $artists_file = file_get_contents('https://docs.google.com/spreadsheets/d/e/2PACX-1vRjHV2Nc4WgrCNjj8j23bpuD3lPfTreeGp006eMMRMwKKHQ6rTIvDmyAru-ND0epGmF4K5LZdDA4TKv/pub?output=csv');

        // $csv = Reader::createFromString($artists_file);
        // $csv->setHeaderOffset(0);
        // $artists = $csv->getRecords();

        // foreach( $artists as $k => $artist ) {
        //     // Skip empty lines
        //     if( empty($artist['name']) ) {
        //         continue;
        //     }

        //     // Slugify
        //     $artist_slug = Slugify::create()->slugify($artist['name']);

        //     // Try to get exact match by slug
        //     $artist_record = $this->app['query']->getContent('artists', [
        //         'slug'         => $artist_slug,
        //         'returnsingle' => true,
        //     ]);

        //     // Try to get fuzzy match
        //     if(empty($artist_record)) {
        //         $artist_id = $this->getArtistId($artist);
        //         if( $artist_id ) {
        //             $artist_record = $this->app['query']->getContent('artists', [
        //                 'id'         => $artist_id,
        //                 'returnsingle' => true,
        //             ]);
        //             dump($artist['name'] . ' || ' . $artist_record['title']);
        //         }
        //     }

        //     // $io->confirm(sprintf('%s == %s, ok ?', $artist['name'], $artist_record['title']), true);


        //     // Artist exists ?
        //     // $logger->info(sprintf('Looking for %s', $artist_slug));


        //     // // Continue if artist already exists
        //     // if( true === !!$artist_record ) {
        //     //     continue;
        //     // }
        //     // $record = $this->app['storage']->getEmptyContent('artists');
        //     // $record->setValues([
        //     //     'datepublish' => empty($artist['datetime']) ? date('Y-m-d H:i:s') : $artist['datetime'],
        //     //     'title'        => $artist['name'],
        //     //     'slug'         => $artist_slug,
        //     //     'datetime'     => empty($artist['datetime']) ? date('Y-m-d H:i:s') : $artist['datetime'],
        //     //     'style'        => $artist['style'],
        //     //     'music_label'  => $artist['music_label'],
        //     //     'origin'       => $artist['origin'],
        //     //     'price'        => $artist['price'],
        //     //     'is_free'      => stripos($artist['price'], 'gratuit') === false ? 0 : 1,
        //     //     'is_cancelled' => $artist['is_cancelled'],
        //     // ]);
        //     // $this->app['storage']->saveContent($record);
        //     // $logger->info(sprintf('Added %s', $artist['name']));
        //     // continue;

        // }

    }

    private function getPlaceId($place) {
        if(empty($place)) {
            return false;
        }
        $fuse = new Fuse($this->places, [
            'keys' => ['title'],
            // 'includeScore' => true,
            'threshold' => .1,
        ]);
        $result = $fuse->search($place);
        // if( isset($result[0]) ) {
        //     dump('ok => ' . $result[0]['title']);
        // } else {
        //     dump('ko => ' . $place);
        // }
        return isset($result[0]['id']) ? intval($result[0]['id']) : false;
    }

    private function getArtistId($artist)
    {
        $fuse = new Fuse($this->artists, [
            'keys' => ['title'],
            // 'includeScore' => true,
            'threshold' => .1,
        ]);
        $result = $fuse->search($artist['name']);
        return isset($result[0]['id']) ? intval($result[0]['id']) : false;
    }
    private function expandHomeDirectory($path)
    {
        $homeDirectory = getenv('HOME');
        if (empty($homeDirectory)) {
            $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
        }
        return str_replace('~', realpath($homeDirectory), $path);
    }
    private function getClient()
    {
        $client = new Google_Client();
        $client->setApplicationName('Google Sheets API PHP Quickstart');
        $client->setScopes(implode(' ', array(
            Google_Service_Sheets::SPREADSHEETS,
            Google_Service_Sheets::DRIVE,
            Google_Service_Sheets::DRIVE_FILE)
        ));
        $client->setAuthConfig(__DIR__ . '/../client_secret.json');
        $client->setAccessType('offline');

        // Load previously authorized credentials from a file.
        $credentialsPath = $this->expandHomeDirectory('credentials.json');
        if (file_exists($credentialsPath)) {
            $accessToken = json_decode(file_get_contents($credentialsPath), true);
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

            // Store the credentials to disk.
            if (!file_exists(dirname($credentialsPath))) {
                mkdir(dirname($credentialsPath), 0700, true);
            }
            file_put_contents($credentialsPath, json_encode($accessToken));
            printf("Credentials saved to %s\n", $credentialsPath);
        }
        $client->setAccessToken($accessToken);

        // Refresh the token if it's expired.
        if ($client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }

}
